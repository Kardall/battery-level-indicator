﻿using System;
using System.IO;
using System.Net;
using SharpAdbClient;
using SharpAdbClient.DeviceCommands;

// Note: You have to have Android SDK Platform-Tools installed. They now install to Local AppData folder.
//       This is currently setup on Windows Environment.

namespace MobileBatteryLevelIndicator
{
    class Program
    {
        static void Main(string[] args)
        {
            // Wait For Device To Be Plugged In
            // Detect iOS or Android (Android For Now)
            // connect to designated device type

            AdbServer server = new AdbServer();
            string adbLocation = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Android", "Sdk", "platform-tools", "adb.exe");
            var result = server.StartServer(@adbLocation, restartServerIfNewer: false);

            DeviceMonitor monitor = new DeviceMonitor(new AdbSocket(new IPEndPoint(IPAddress.Loopback, AdbClient.AdbServerPort)));
            
            monitor.DeviceConnected += OnDeviceConnected;
            monitor.DeviceChanged += Monitor_DeviceChanged;
            monitor.Start();

            while (true)
            {
                // Keep Open
            }
        }

        // This is called while program is running and you unplug/plug in a device.
        private static void Monitor_DeviceChanged(object sender, DeviceDataEventArgs e)
        {
            if (e.Device.State == DeviceState.Online)
            {
                var recv = new ConsoleOutputReceiver();

                DeviceExtensions.ExecuteShellCommand(e.Device, "dumpsys battery", recv);
                recv.Flush();
                Console.WriteLine(recv);
            }
        }

        // This seems to get fired on program run.
        private static void OnDeviceConnected(object sender, DeviceDataEventArgs e)
        {
            if(e.Device.State == DeviceState.Online)
            {
                var recv = new ConsoleOutputReceiver();
                // Issue Command to get status via shell command
                DeviceExtensions.ExecuteShellCommand(e.Device, "dumpsys battery", recv);
                recv.Flush();
                Console.WriteLine(recv);
            }
        }
    }
}
